# 小说解析源格式

## 示例：

```json
{
  "_id": "97416845-fcf0-458f-b454-40d18eb59cea",
  "url": "www.xiaoshuo530.com",
  "name": "小说书网",
  "order": 30.0,
  "enable": true,
  "pageSize": 10.0,
  "search": {
    "url": "https://www.xiaoshuo530.com/index.php?",
    "searchData": { "extraData": { "s": "/web/index/search" } },
    "method": "post",
    "result": {
      "data": {
        "type": "array",
        "selector": "div#main>div.novelslist2>ul>li",
        "result": {
          "name": {
            "type": "selector",
            "selector_handle": [
              { "args": "span.s2>a", "type": "function", "function": "find" },
              { "type": "function", "function": "text" }
            ]
          },
          "author": {
            "type": "selector",
            "selector_handle": [
              { "type": "function", "function": "find", "args": "span.s4>a" },
              { "function": "text", "type": "function" }
            ]
          },
          "latest_chapter_name": {
            "type": "selector",
            "selector_handle": [{ "function": "find", "args": "span.s3>a", "type": "function" }],
            "attr": "title"
          },
          "latest_chapter_url": {
            "selector_handle": [{ "type": "function", "function": "find", "args": "span.s3>a" }],
            "attr": "href",
            "baseUrl": "https://www.xiaoshuo530.com",
            "type": "selector"
          },
          "read_url": {
            "selector_handle": [{ "type": "function", "function": "find", "args": "span.s2>a" }],
            "attr": "href",
            "baseUrl": "https://www.xiaoshuo530.com",
            "type": "selector"
          },
          "update_time": {
            "type": "selector",
            "selector_handle": [
              { "type": "function", "function": "find", "args": "span.s6" },
              { "type": "function", "function": "text" }
            ]
          }
        },
        "offset": "1"
      }
    },
    "postData": { "key": "name" }
  },
  "detail": {
    "result": {
      "author": { "selector": "meta[property=\"og:novel:author\"]", "attr": "content" },
      "latest_chapter_url": {
        "baseUrl": "https://www.xiaoshuo530.com",
        "selector": "meta[property=\"og:novel:latest_chapter_url\"]",
        "attr": "content"
      },
      "read_url": {
        "selector": "meta[property=\"og:novel:read_url\"]",
        "attr": "content",
        "baseUrl": "https://www.xiaoshuo530.com"
      },
      "description": { "attr": "content", "selector": "meta[property=\"og:description\"]" },
      "cover": { "selector": "meta[property=\"og:image\"]", "attr": "content", "baseUrl": "https://www.xiaoshuo530.com" },
      "name": { "selector": "meta[property=\"og:novel:book_name\"]", "attr": "content" },
      "category": { "selector": "meta[property=\"og:novel:category\"]", "attr": "content" },
      "update_time": { "attr": "content", "selector": "meta[property=\"og:novel:update_time\"]" },
      "first_chapter_url": {
        "attr": "href",
        "baseUrl": "https://www.xiaoshuo530.com",
        "type": "selector",
        "selector_handle": [{ "function": "eq", "args": 12.0, "type": "function" }],
        "selector": "div#list>dl>dd>a"
      },
      "latest_chapter_name": { "selector": "meta[property=\"og:novel:latest_chapter_name\"]", "attr": "content" }
    }
  },
  "catalogs": {
    "result": {
      "catalogs": {
        "type": "array",
        "offset": 12.0,
        "selector": "div#list>dl>dd>a",
        "result": {
          "name": { "type": "function", "function": "text" },
          "url": { "attr": "href", "baseUrl": "https://www.xiaoshuo530.com" }
        }
      },
      "name": { "attr": "content", "selector": "meta[property=\"og:novel:book_name\"]" }
    }
  },
  "content": {
    "result": {
      "prev": {
        "selector": "div#main>div.content_read>div.box_con>div.bottem2>a",
        "type": "selector",
        "attr": "href",
        "baseUrl": "https://www.xiaoshuo530.com",
        "selector_handle": [{ "function": "eq", "args": "1", "type": "function" }]
      },
      "next": {
        "type": "selector",
        "attr": "href",
        "baseUrl": "https://www.xiaoshuo530.com",
        "selector_handle": [{ "type": "function", "function": "eq", "args": 3.0 }],
        "selector": "div#main>div.content_read>div.box_con>div.bottem2>a"
      },
      "name": { "function": "text", "selector": "div#main>div.content_read>div.box_con>div.bookname>h1", "type": "function" },
      "content": {
        "type": "function",
        "function": "html",
        "br2n": true,
        "selector": "div#content",
        "tag2s": ["p", { "start": "div align=\"center\"", "end": "div" }]
      }
    }
  }
}
```

## 字段含义

| 字段     | 含义         | 类型    | 备注                       |
| -------- | ------------ | ------- | -------------------------- |
| url      | 站点地址     | string  | 不包含 http 头的域名地址   |
| name     | 站点名称     | string  | 站点的中文名称             |
| enable   | 是否启用该源 | boolean | 是否启用该解析源           |
| order    | 排序         | int     | 小程序中解析源列表中的排序 |
| pageSize | 分页大小     | int     | 站点中搜索查询时的分页大小 |
| search   | 爬虫规则     | object  | 搜索查询时的爬虫规则       |
| detail   | 爬虫规则     | object  | 查询书籍详情时的爬虫规则   |
| catalogs | 爬虫规则     | object  | 查询书籍目录时的爬虫规则   |
| content  | 爬虫规则     | object  | 看书时，查询内容的爬虫规则 |

## 爬虫规则

爬虫规则的格式是一个 json 对象

### 爬虫地址

- **url**
  爬虫请求的 url 基本地址。  
  比如查询书籍详情目录等爬虫规则中没有此字段，是因为云函数代码中执行爬虫函数的时候传入了相应的 url 地址。  
  一般小说站点都会有搜索的功能，此字段为搜索结果的的页面地址的前面不带参数部分。注意格式，如果地址有参数的话，最后需要有个?号。
- **method**
  请求的方法。
  一般时 get 或者 post
- **searchData**
  爬虫请求时的路径参数。  
  此字段包含两个字段：
  - `key` 搜索的值对应的字段名称
  - `extraData` 额外附加的字段以及对应的值
- **type**
  type=gbk 时，小说源的页面编码时 gbk 格式，爬虫会根据此字段进行转换编码格式
- **postData**
  当请求方法为 post 时的 body 参数。

1. 对于下述爬虫规则示例：

   ```json
   {
     "url": "https://www.xiaoshuo530.com/index.php?",
     "searchData": { "extraData": { "s": "/web/index/search" } },
     "method": "post",
     "postData": { "key": "name" }
   }
   ```

   那么爬虫的请求就是
   `curl -d "name=搜索值" -X POST https://www.xiaoshuo530.com/index.php?s=/web/index/search`

2. 再比如下述爬虫规则示例：

   ```json
   {
     "url": "https://www.xiaoshuozu8.com/s.php?",
     "searchData": { "key": "q", "extraData": { "ie": "gbk" }, "type": "gbk" },
     "type": "gbk"
   }
   ```

   那么爬虫的请求就是
   `curl https://www.xiaoshuozu8.com/s.php?ie=gbk&q=%CB%D1%CB%F7%D6%B5`
   `searchData` 中的 `type=gbk` 的含义是对请求参数 `ie=gbk&q=搜索值` 进行 gbk 编码转换

### 爬虫结果

爬虫结果的解析规则在 `result` 字段  
如果所爬取的页面编码方式是 gbk 的话，那么需要在 result 的统计字段加 type=gbk

`result`字段中包含每个返回结果字段的解析规则

- 搜索查询书籍`search`的爬虫规则，返回结果只有一个`data`字段
  data 字段的结果是一个数组，是一个所搜索数据的结果列表
- 查询书籍详情`detail`的爬虫规则，返回字段有：
  - `name` 书籍名称
  - `description` 书籍简介
  - `category` 书籍分类
  - `update_time` 书籍最新更新时间
  - `first_chapter_url` 书籍第一章的阅读地址
  - `latest_chapter_url` 书籍最新一章的阅读地址
  - `author` 作者
  - `status` 书籍状态，完本/连载
  - `read_url` 阅读地址
  - `cover` 书籍封面图片
  - `latest_chapter_name` 书籍最新一章的章节名称
- 查询书籍目录`catalogs`的爬虫规则，返回字段有：
  - `catalogs` 书籍目录，是一个数组
  - `name` 书籍名称
- 查询书籍阅读内容`content`的爬虫规则，返回字段有：
  - `prev` 上一章的阅读地址
  - `next` 下一章的阅读地址
  - `name` 本章节名称
  - `content` 阅读内容

上述这些字段都是固定的，不可更改。更改的是每个字段的结果解析规则

### 解析规则

解析规则是一个 json 对象

1. 示例 1
   ```json
   {
     "name": { "attr": "content", "selector": "meta[property=\"og:novel:book_name\"]" }
   }
   ```
   解析规则是
   ```javascript
   const name = $('meta[property="og:novel:book_name"]').attr('content');
   ```
2. 示例 2
   ```json
   {
     "content": {
       "br2n": true,
       "selector": "div#content",
       "type": "function",
       "function": "html"
     }
   }
   ```
   解析规则是
   ```javascript
   const content = $('div#content')
     .html()
     .replace(/(<br\s?\/?>|\n)+((\s+\n+)|(\s+<br\s?\/?>+)|\n+|<br\s?\/?>+)*/gi, '\n\n');
   // replace是把<br/>替换为\n
   ```
3. 示例 3
   ```json
   {
     "name": { "function": "text", "selector": "div#book>div.content>h1", "type": "function" }
   }
   ```
   解析规则是
   ```javascript
   const name = $('div#book>div.content>h1').text();
   ```
4. 示例 4

   ```json
   {
     "prev": {
       "selector": "div#book>div.content>div.page_chapter>ul>li>a",
       "attr": "href",
       "type": "selector",
       "selector_handle": [{ "type": "function", "function": "eq", "args": "0" }],
       "baseUrl": "https://www.xiaoshuozu8.com"
     }
   }
   ```

   解析规则是

   ```javascript
   const prev =
     'https://www.xiaoshuozu8.com' +
     $('div#book>div.content>div.page_chapter>ul>li>a')
       .eq(0)
       .attr('href');
   // 先执行selector_handle中的函数获取到目标元素
   ```

5. 示例 5
   ```json
   {
     "latest_chapter_url": {
       "baseUrl": "https://www.xiaoshuo530.com",
       "selector": "meta[property=\"og:novel:latest_chapter_url\"]",
       "attr": "content"
     }
   }
   ```
   解析规则是
   ```javascript
   const latest_chapter_url = 'https://www.xiaoshuo530.com' + $('meta[property="og:novel:latest_chapter_url"]').attr('content');
   ```
6. 示例 6
   ```json
   {
     "cover": {
       "baseUrl": "https://www.xiaoshuozu8.com",
       "attr": "src",
       "type": "selector",
       "selector_handle": [{ "type": "function", "function": "find", "args": "div.p10>div.bookimg>a>img" }]
     }
   }
   ```
   解析规则是
   ```javascript
   const cover = 'https://www.xiaoshuozu8.com' + $('meta[property="og:novel:latest_chapter_url"]').attr('content');
   ```
7. 示例 7
   ```json
   {
     "data": {
       "selector": "div.result-list>div.result-item.result-game-item",
       "type": "array",
       "offset": 12.0,
       "result": {
         "update_time": {
           "type": "selector",
           "selector_handle": [
             {
               "type": "function",
               "function": "find",
               "args": "div.result-game-item-detail>div.result-game-item-info>p.result-game-item-info-tag>span.result-game-item-info-tag-title"
             },
             { "function": "eq", "args": 4, "type": "function" },
             { "function": "text", "type": "function" },
             { "function": "trim", "type": "function" }
           ]
         }
       }
     }
   }
   ```
   解析规则是
   ```javascript
   const data = [];
   $('div.result-list>div.result-item.result-game-item').each(function(index, item) {
     if (index > 12.0) {
       const update_time = $(item)
         .find(
           'div.result-game-item-detail>div.result-game-item-info>p.result-game-item-info-tag>span.result-game-item-info-tag-title'
         )
         .eq(4)
         .text()
         .trim();
       data.push({ update_time });
     }
   });
   ```

可参照数据库中的解析规则修改部分字段即可
[database_export.json](https://gitee.com/wtto00/dt-read/blob/master/src/static/database_export.json)
